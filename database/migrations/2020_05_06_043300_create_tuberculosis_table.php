<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTuberculosisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tuberculoses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('year_id')->unsigned();
            $table->foreign('year_id')
                    ->references('id')
                    ->on('years');
            $table->bigInteger('subdistrict_id')->unsigned();
            $table->foreign('subdistrict_id')
                    ->references('id')
                    ->on('subdistricts');
            $table->string('case');
            $table->string('cnr');
            $table->string('population');
            $table->string('target_case');
            $table->string('mortality_rate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tuberculoses');
    }
}
