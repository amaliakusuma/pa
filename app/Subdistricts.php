<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subdistricts extends Model
{
    public function tuberculosis() {
        return $this->hasmany(Tuberculosis::class);
    }

    protected $fillable = [
        'subdistrict_name',
        'longitude',
        'latitude',
    ];

}
