<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthcareFacility extends Model
{
    protected $fillable = [
        'institution_name',
        'institution_address',
    ];
}
