<?php

namespace App;

use App\Subdistricts;
use App\Year;
use Illuminate\Database\Eloquent\Model;

class Tuberculosis extends Model
{
    public function subdistrict()
    {
        return $this->belongsTo(Subdistricts::class);
    }

    public function year()
    {
        return $this->belongsTo(Year::class);
    }

    protected $fillable = [
        'year_id',
        'subdistrict_id',
        'case',
        'cnr',
        'population',
        'target_case',
        'mortality_rate',
    ];
}
