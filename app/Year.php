<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    public function tuberculosis() {
        return $this->hasmany(Tuberculosis::class);
    }

    protected $fillable = [
        'year'
    ];
}
