<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TuberculosisResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tuberculosis = [
            'id' => $this->id,
            'case' => $this->case,
            'cnr' => $this->cnr,
            'population' => $this->population,
            'target_case' => $this->target_case,
            'mortality_rate' => $this->mortality_rate,
            'year_id' => $this->year,
            'subdistrict_id' => $this->subdistrict,
        ];

        return $tuberculosis;
    }
}
