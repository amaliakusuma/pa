<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HealthcareFacilityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $facilities = [
            'id'=> $this->id,
            'institution_name' => $this->institution_name,
            'institution_address' => $this->instution_address,
        ];

        return $facilities;
    }
}
