<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TuberculosisInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $information = [
            'id' => $this->id,
            'description' => $this->description,
            'transmission' => $this->transmission,
            'risk_factor' => $this->risk_factor,
            'symptoms' => $this->symptoms,
            'prevention' => $this->prevention,
        ];

        return $information;
    }
}
