<?php

namespace App\Http\Controllers;

use App\Year;
use Illuminate\Http\Request;

class YearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Year::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $year = new Year;

        $year->year = $request->get('year');

        if($year->save())
            return response()->json([
                'success' => true,
                'year' => $year
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Sorry, years could not be added.'
            ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Year  $year
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $year = Year::find($id);

        if(!$year)
            throw new NotFoundHttpException;

        return $year;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Year  $year
     * @return \Illuminate\Http\Response
     */
    public function edit(Year $year)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Year  $year
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $year = Year::find($id);

        if (!$year) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, year with id ' . $id . ' cannot be found.'
            ], 400);
        }

        $updated = $year->fill($request->all())->save();

        if ($updated) {
            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, year could not be updated.'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Year  $year
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $year = Year::find($id);

        if (!$year) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, year with id ' . $id . ' cannot be found.'
            ], 400);
        }

        if ($year->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'year could not be deleted.'
            ], 500);
        }
    }
}
