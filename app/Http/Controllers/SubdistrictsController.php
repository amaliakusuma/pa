<?php

namespace App\Http\Controllers;

use App\Subdistricts;
use Illuminate\Http\Request;

class SubdistrictsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Subdistricts::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subdistrict = new Subdistricts;

        $subdistrict->subdistrict_name = $request->get('subdistrict_name');
        $subdistrict->longitude = $request->get('longitude');
        $subdistrict->latitude = $request->get('latitude');

        if($subdistrict->save())
            return response()->json([
                'success' => true,
                'subdistrict' => $subdistrict
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Sorry, subdistricts could not be added.'
            ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subdistricts  $subdistricts
     * @return \Illuminate\Http\Response
     */
    public function show(Subdistricts $subdistricts, $id)
    {
        $subdistrict = Subdistricts::find($id);

        if(!$subdistrict)
            throw new NotFoundHttpException;

        return $subdistrict;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subdistricts  $subdistricts
     * @return \Illuminate\Http\Response
     */
    public function edit(Subdistricts $subdistricts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subdistricts  $subdistricts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subdistricts $subdistricts, $id)
    {
        $subdistrict = Subdistricts::find($id);

        if (!$subdistrict) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, subdistrict with id ' . $id . ' cannot be found.'
            ], 400);
        }

        $updated = $subdistrict->fill($request->all())->save();

        if ($updated) {
            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, subdistrict could not be updated.'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subdistricts  $subdistricts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subdistricts $subdistricts, $id)
    {
        $subdistrict = Subdistricts::find($id);

        if (!$subdistrict) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, subdistrict with id ' . $id . ' cannot be found.'
            ], 400);
        }

        if ($subdistrict->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'subdistrict could not be deleted.'
            ], 500);
        }
    }
}
