<?php

namespace App\Http\Controllers;

use App\HealthcareFacility;
use App\Http\Resources\HealthcareFacilityResource;
use Illuminate\Http\Request;

class HealthcareFacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return HealthcareFacilityResource::collection(HealthcareFacility::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $healthcareFacility = new HealthcareFacility;

        $healthcareFacility->institution_name = $request->get('institution_name');
        $healthcareFacility->institution_address = $request->get('institution_address');

        if($healthcareFacility->save())
            return response()->json([
                'success' => true,
                'information' => $healthcareFacility
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Sorry, institution could not be added.'
            ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HealthcareFacility  $healthcareFacility
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $healthcareFacility = HealthcareFacility::find($id);

        if(!$healthcareFacility)
            throw new NotFoundHttpException;

        return $healthcareFacility;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HealthcareFacility  $healthcareFacility
     * @return \Illuminate\Http\Response
     */
    public function edit(HealthcareFacility $healthcareFacility)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HealthcareFacility  $healthcareFacility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $healthcareFacility = HealthcareFacility::find($id);

        if (!$healthcareFacility) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, institution with id ' . $id . ' cannot be found.'
            ], 400);
        }

        $updated = $healthcareFacility->fill($request->all())->save();

        if ($updated) {
            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, information could not be updated.'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HealthcareFacility  $healthcareFacility
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $healthcareFacility = HealthcareFacility::find($id);

        if (!$healthcareFacility) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, institution with id ' . $id . ' cannot be found.'
            ], 400);
        }

        if ($healthcareFacility->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'institution could not be deleted.'
            ], 500);
        }
    }
}
