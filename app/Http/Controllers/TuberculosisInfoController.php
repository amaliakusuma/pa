<?php

namespace App\Http\Controllers;

use App\TuberculosisInfo;
use App\Http\Resources\TuberculosisInfoResource;
use Illuminate\Http\Request;

class TuberculosisInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TuberculosisInfoResource::collection(TuberculosisInfo::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $information = new TuberculosisInfo;

        $information->description = $request->get('description');
        $information->information = $request->get('information');

        if($information->save())
            return response()->json([
                'success' => true,
                'information' => $information
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Sorry, information could not be added.'
            ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TuberculosisInfo  $tuberculosisInfo
     * @return \Illuminate\Http\Response
     */
    public function show(TuberculosisInfo $tuberculosisInfo, $id)
    {
        $information = TuberculosisInfo::find($id);

        if(!$information)
            return response()->json([
                'success' => false,
                'message' => 'Sorry, information with id ' . $id . ' cannot be found.'
        ], 400);

        return $information;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TuberculosisInfo  $tuberculosisInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(TuberculosisInfo $tuberculosisInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TuberculosisInfo  $tuberculosisInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TuberculosisInfo $tuberculosisInfo, $id)
    {
        $information = TuberculosisInfo::find($id);

        if (!$information) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, information with id ' . $id . ' cannot be found.'
            ], 400);
        }

        $updated = $information->fill($request->all())->save();

        if ($updated) {
            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, information could not be updated.'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TuberculosisInfo  $tuberculosisInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(TuberculosisInfo $tuberculosisInfo, $id)
    {
        $information = TuberculosisInfo::find($id);

        if (!$information) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, information with id ' . $id . ' cannot be found.'
            ], 400);
        }

        if ($information->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'information could not be deleted.'
            ], 500);
        }
    }
}
