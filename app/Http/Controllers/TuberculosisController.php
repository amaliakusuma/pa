<?php

namespace App\Http\Controllers;

use App\Tuberculosis;
use App\Subdistricts;
use App\Year;
use App\Http\Resources\TuberculosisResource;
use Illuminate\Http\Request;

class TuberculosisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TuberculosisResource::collection(Tuberculosis::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tuberculosis = new Tuberculosis;

        $tuberculosis->case = $request->get('case');
        $tuberculosis->cnr = $request->get('cnr');
        $tuberculosis->population = $request->get('population');
        $tuberculosis->target_case = $request->get('target_case');
        $tuberculosis->mortality_rate = $request->get('mortality_rate');
        $tuberculosis->year_id = $request->get('year_id');
        $tuberculosis->subdistrict_id = $request->get('subdistrict_id');

        if($tuberculosis->save())
            return response()->json([
                'success' => true,
                'tuberculosis' => $tuberculosis
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Sorry, tuberculosis could not be added.'
            ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tuberculosis  $tuberculosis
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tuberculosis = Tuberculosis::find($id);

        if(!$tuberculosis)
            throw new NotFoundHttpException;

        return $tuberculosis;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tuberculosis  $tuberculosis
     * @return \Illuminate\Http\Response
     */
    public function edit(Tuberculosis $tuberculosis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tuberculosis  $tuberculosis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tuberculosis $tuberculosis, $id)
    {
        $tuberculosis = Tuberculosis::find($id);

        if (!$tuberculosis) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, tuberculosis with id ' . $id . ' cannot be found.'
            ], 400);
        }

        $updated = $tuberculosis->fill($request->all())->save();

        if ($updated) {
            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, tuberculosis could not be updated.'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tuberculosis  $tuberculosis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tuberculosis $tuberculosis, $id)
    {
        $tuberculosis = Tuberculosis::find($id);

        if (!$tuberculosis) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, tuberculosis with id ' . $id . ' cannot be found.'
            ], 400);
        }

        if ($tuberculosis->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'tuberculosis could not be deleted.'
            ], 500);
        }
    }
}
