<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TuberculosisInfo extends Model
{
    protected $fillable = [
        'description',
        'information',
    ];
}
