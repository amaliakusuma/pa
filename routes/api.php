<?php

use Illuminate\Http\Request;

Route::post('auth/login', 'AuthController@login');
Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'auth'
], function ($router) {
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group([
    'middleware' => 'jwt.auth',
], function ($router) {
    Route::resources([
        'subdistricts' => 'SubdistrictsController',
        'years' => 'YearController',
        'tuberculosis' => 'TuberculosisController',
        'catalog' => 'TuberculosisInfoController',
        'healthcare-facility' => 'HealthcareFacilityController'
    ]);
});
